package comprehensive;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.Scanner;

/**
 * Uses a HashMap to sort the nonterminal values of a grammar file. Contains
 * a couple crucial public methods, the constructor and getPhrase method.
 * 
 * @author David Onken
 *
 */
public class PhraseMaker {
	
	private HashMap<String, String[]> base;
	private Random rng;
	
	// Testing variable
	//public int innerCount;

	/**
	 * Takes in a filename and uses the linked file to build the hashmap for
	 * the phrasemaker. Once done, the phrasemaker can build a phrase whenever
	 * requested to.
	 * 
	 * @param filename
	 */
	public PhraseMaker(String filename)
	{
		// Mandatory Variables
		base = new HashMap<String, String[]>();
		rng = new Random();
		
		// Testing variable
		//innerCount = 0;
		
		// Enclose everything in a try/catch block.
		try {
			// Make a new Scanner object from the file.
			Scanner fileIn = new Scanner(new File(filename));
			String line;
			
			// While we still have lines, continue this loop
			while (fileIn.hasNextLine()) {
				// Read the next line as a string.
				line = fileIn.nextLine();
				
				if (line.trim().equals("{"))
					parseNonterminal(fileIn);
			}
			
			fileIn.close();
			
		} catch (FileNotFoundException e) { System.out.println(filename + " does not exist."); }
		
		
	}
	
	/**
	 * Takes in the scanner object and builds the Array inside the hashMap for the 
	 * nonterminal definition.
	 * 
	 * @param file
	 */
	private void parseNonterminal(Scanner file)
	{
		// Get the current line and trim it.
		String currLine = file.nextLine().trim();
		String nonTerm = null;
		
		
		// Check if the next line is a opening bracket
		if (currLine.charAt(0) == '<')
		{
			nonTerm = currLine;
		}
		else
		{
			System.out.println("Error in parsing.");
			return;
		}
		
		ArrayList<String> list = new ArrayList<String>();
		currLine = file.nextLine();
		
		// Until the line is a closing bracket, keep adding the lines to the list
		while (currLine.charAt(0) != '}')
		{
			list.add(currLine);
			currLine = file.nextLine();
		}
		
		// Make an array of the size of the list
		String[] input = new String[list.size()];
		
		// Now transfer the values from the list to the array
		for (int i = 0; i < list.size(); i++)
			input[i] = list.get(i);
			
		// Put the array of values into the hashmap at the nonterm
		base.put(nonTerm, input);
				
	}
	
	/**
	 * Takes in a nonterminal target word (like start) and then returns a 
	 * string containing a randomized item out of the nonterminal definition.
	 * 
	 * By happen stance, this is also the driver method for the whole getPhase method.
	 * 
	 * By complete accident I realized that randomTerminal's recursive getPhrase call
	 * was doing the entire job of generating the start command by itself, voiding 
	 * a later call by getPhrase on start.
	 * 
	 * @param target
	 * @return A string containing a random item from the nonterminal definiton
	 */
	private String randomTerminal(String target)
	{
		// Make a list of the items at the target.
		String[] list = base.get(target);
		
		// Get a random item in the list and return it.
		String item = list[rng.nextInt(list.length)];
		
		// Use a bit of recursion here. Call getPhrase() on the random terminal.
		// This will return a string with no nonterminals according to abstraction.
		item = getPhrase(item);
		
		// Testing Stuff
		//innerCount++;
		
		return item;
	}
	
	/**
	 * The helper method for the entire getPhrase method collection.
	 * 
	 * Takes in a string and builds a new string character by character. Should 
	 * it ever encounter an angle bracket (<), then it will build a new string and 
	 * pass it to the randomTerminal method, which will return a new string without 
	 * any nonterminals to this method. Once it is returned, add the string to the 
	 * output string and continue.
	 * 
	 * Once finished, it returns a derived string without any nonterminals, while at the same 
	 * time preserving all formatting in the start method.
	 * 
	 * Regardless of its efficiency or coding oddity, I'm quite proud of this.
	 * 
	 * @param line
	 * @author Dave Onken
	 * @return
	 */
	private String getPhrase(String line)
	{
		
		// Build a new empty string
		String output = "";
		
		// Set a boolean and empty char too
		char curr = ' ';

		// Loop through the entire line.
		for (int i= 0; i < line.length(); i++)
		{
			// Set the current char
			curr = line.charAt(i);
			
			// If the char isn't an opening nonterm bracket, then add the char to the output line
			if (!(curr == '<') )
			{
				output = output + line.charAt(i);
			}
			else
			{
				
				String temp = "";
				
				// Loop until we find the end of the nonterm
				while (curr != '>')
				{
					// Add each current char to the temp
					temp = temp + curr;
					
					// If there is no end to the nonterm, then break so the error will be thrown
					if (i >= line.length()-1)
						break;
					
					// Step i forward and grab the char at that point
					curr = line.charAt(++i);
				}
				
				// Finish off the nonterm by adding a closing bracket
				temp = temp + ">";
				
				// If the list doesn't contain the key (the nonterminal), then kick out an error
				if (!base.containsKey(temp))
					return "Illegal File format. A requested nonterminal does not exist.";
				
				// Finally, take our nonterm value and get a random string from it, add it to the line
				output = output + randomTerminal(temp);
			}
		}
		
		// Return the output string!
		return output;
		
	}
	
	public String getPhrase()
	{
		// Make sure that the <start> nonterm exists before calling the helper method
		if (!base.containsKey("<start>"))
				return "Illegal file. Could not build a phrase from the improperly formatted file.";
		
		// Return the completed string from <start>. This will recursively result in success.
		return randomTerminal("<start>");
	}

}
