package comprehensive;

public class PhraseTester {

	public static void main(String[] args) {

		testPhrase("super_simple.g");

		testPhrase("assignment_extension_request.g");
				
		testPhrase("fox_news_headline.g");
		
		testPhrase("mathematical_expression.g");
		
		testPhrase("poetic_sentence.g");
		
		testPhrase("planet_facts.g");

		/*filename = "test.g";

		getRandomPhrases(filename, 5);

		System.out.println();

		filename = "nontermtest.g";

		getRandomPhrases(filename, 5);*/

		// Do 1000 lookups and use the average running time.
		//int timesToLoop = 1000;

		// For each problem size n . . .
		/*for (int n = 1000; n <= 50000; n += 1000) {

			// Generate a new "random" integer set of size n.

			// Build the PhraseMaker object
			PhraseMaker tester = new PhraseMaker("assignment_extension_request.g");
			int total = 0;
			
			long startTime, midpointTime, stopTime;

			// First, spin computing stuff until one second has gone by.
			// This allows this thread to stabilize.
			startTime = System.nanoTime();
			while (System.nanoTime() - startTime < 1000000000) { // empty block
			}

			// Now, run the test.
			startTime = System.nanoTime();

			// Test the getPhrase method
			for (int i = 0; i < n; i++) {

				tester.getPhrase();
				//total += tester.innerCount;
				//tester.innerCount = 0;
			
			}

			midpointTime = System.nanoTime();

			// Run a loop to capture the cost of running the "timesToLoop" loop
			// and
			// generating a random ISBN.
			for (long i = 0; i < n; i++) {

			}

			stopTime = System.nanoTime();

			// Compute the time, subtract the cost of running the loop
			// from the cost of running the loop and doing the lookups.
			// Average it over the number of runs.
			double averageTime = ((midpointTime - startTime) - (stopTime - midpointTime))
					/ n;
			
			//total /= n;
			
			//averageTime /= total;

			System.out.println(n + "\t" + averageTime);

		}*/

		System.out.println();
		System.out.println("Testing Done");

	}
	
	private static void testPhrase(String filename)
	{
		getRandomPhrases(filename, 5);
	}

	private static void getRandomPhrases(String filename, int times) {

		PhraseMaker phrase = new PhraseMaker(filename);

		System.out.println("-----------" + filename + " tests-----------");
		for (int i = 0; i < times; i++)
			System.out.println(phrase.getPhrase());

	}

}
