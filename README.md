**Quick summary:**

Uses .g files to generate a completely random sentence. Only one of the original .g files has been included with this program.

This was made for CS 2420, one of my early CS courses.

**How to Use:**

Run a standard compilation on the .java files using a 1.7 compiler. The resulting 
program is run with two command line arguments. The first argument is a string 
representing a .g file (defined in the class as a grammar line with strict rules) and a 
number (how many phrases to generate). Using the file, a random sentence will be generated and output to the terminal.