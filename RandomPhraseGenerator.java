package comprehensive;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class RandomPhraseGenerator {

	public static void main(String[] args) {
		
		getRandomPhrases(args[0], Integer.parseInt(args[1]));
		
		// Generate frame.
		//JFrame frame = new JFrame("Phrase Gen");
		//int n;
		
		//do {
			//String arg = JOptionPane.showInputDialog("Input name of file. (with .g):");
			//if (arg == null)
			//	System.exit(0);
		//	
		//	String arg = "";
		//		
		//	int num = Integer.parseInt(JOptionPane.showInputDialog("Input the number of virtual Dexisms to be generated:"));
		//	
		//	if (arg == null || num < 1)
		//		System.exit(0);
		//	
		//	JOptionPane.showMessageDialog(frame, getRandomPhrases("virtual_dex.g", num));
		//	Object[] options = { "Yes", "No" };
		//	n = JOptionPane
		//			.showOptionDialog(
		//					frame,
		//					"Would you like to generate another phrase?",
		//					"Generate Again?", JOptionPane.YES_NO_OPTION,
		//					JOptionPane.QUESTION_MESSAGE, null, options,
		//					options[0]);
		//	
		//} while (n == JOptionPane.YES_OPTION);
		
		
		System.exit(0);

	}

	private static String getRandomPhrases(String filename, int times) {

		PhraseMaker phrase = new PhraseMaker(filename);
		
		String ret = "";

		System.out.println("-----------" + filename + " Phrases-----------");
		for (int i = 0; i < times; i++)
			System.out.println(phrase.getPhrase());
		
		for (int i = 0; i < times; i++)
			ret += phrase.getPhrase() + "\n";
		
		try {
			PrintWriter out = new PrintWriter("phrases.txt");
			out.println(ret);
			out.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		ret += "\nThese phrases have been written to the 'phrases.txt' file in this folder as well.\n";
		
		return ret;
		
	}
	
	

}
